import React from "react"
import { Route, Redirect } from "react-router-dom"
import { useAuth } from "./useAuth"

export default ({path, Component, loginPath = "/login"}) => {
    const auth = useAuth();
    return (
        <Route
        path={path}
        component={() =>
          auth.autenticado ? <Component /> : <Redirect to={loginPath} />
        }
      />  
    );
}