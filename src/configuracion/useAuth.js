import React, { useContext, useState, createContext } from "react";
import axios from "axios";
import validarSesionActiva from "./validarSesionActiva";

export const Context = createContext({});

export const AuthProvider = props => {
const sesion = validarSesionActiva
  const [autenticado, setAutenticado] = useState(sesion)
  const login = async (user, callback) => {
    const res = await axios.post(
      "https://login-test-dga.herokuapp.com/login",
      user
    );

    if (res.data.response) {
      localStorage.setItem("username", user.username);
      localStorage.setItem("password", user.password);

      setAutenticado(true);
      callback();
    }
  };

  const logout = callback => {
    localStorage.setItem("username", "");
    localStorage.setItem("password", "");

    setAutenticado(false);
    callback();
  };
  const [encuestaFin, setEncuestaFin] = useState(false);
  const manejoFinish = valor => {
    setEncuestaFin(valor);
    alert("Encuesta finalizada con éxito   " + encuestaFin.toString());
  };
  return (
    <Context.Provider value={{ manejoFinish, login, autenticado, logout }}>
      {props.children}
    </Context.Provider>
  );
};

export const useAuth = () => {
  return useContext(Context);
};
