import React, {useState} from "react"
import Select from "react-select"
import { Boton } from "../../containers/Home/styles"
import { Formik, Form } from "formik"
import { useAuth } from "../../configuracion/useAuth";

const SelectOp = ({ callback, contenido, index, length }) => {

const contextoFin = useAuth();
  
const [respEncuesta, setRespEncuesta] = useState(false);

const opcionesEncuesta = contenido.map(content =>({label: content.nombreOpcion, value: content.esVerdadera}))
let indexSelect= index;
console.log("en INDICES", indexSelect, length )
const handleChange = (selectedOp) => {
  let esTrue = null;
  opcionesEncuesta.filter(element => element.label === selectedOp.label && (esTrue = element.value));
  setRespEncuesta(esTrue);
  };

 return (  
  <Formik initialValues={{ opcionesEncuesta, selecccion: "" }}
      validate={campoSelect => {!campoSelect.seleccion && alert("Aun existen preguntas sin responder, seleccione una respuesta");}}
      onSubmit={() =>
        parseInt(index) + 1 !== length
          ? callback(parseInt(index) + 1)
          : contextoFin.manejoFinish(true)
        }
  
>{({ values, handleSubmit }) =>(
<Form>
 <Select onChange={handleChange} placeholder="Seleccione la respuesta correcta" options={opcionesEncuesta}></Select>
{(parseInt(indexSelect)+1) !== length ? <Boton onClick={handleSubmit} >Enviar respuesta</Boton> : <Boton onClick={handleSubmit} >Finalizar</Boton>}
  {respEncuesta ? 
  (<span style={{"color":"green"}}>La respuesta Seleccionada es Correcta :)</span>)
   :  (<span style={{"color":"red"}}>La respuesta Seleccionada es Incorrecta :(</span>) 
}
  </Form>
      )}
  </Formik>
);
};
export default SelectOp;

