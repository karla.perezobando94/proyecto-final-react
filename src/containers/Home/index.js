import React, {useState, createContext} from "react"
import { useHistory } from "react-router-dom"
import { Formik, Form } from "formik";
import { Wrapper } from "../Home/styles"
import { useAuth } from "../../configuracion/useAuth"
import Container from "../../component/Container"
import SelectOp from "../../component/Select"
import { encuesta } from "../../encuesta"
import { WrapperContainer} from "./styles"

export const ContextoFin = createContext({});

  
export default () => {
const [itemActual, setItemActual] = useState(0)
const seleccionado = (index) => setItemActual(index)


  const auth = useAuth();
  const history = useHistory();

    return(
      <div>
      <Wrapper>
        <h3>¡Bienvenid@!</h3>  
        <button
          onClick={() => {
          auth.logout(() => history.push("/login"));
          }}
        >
        Cerrar Sesion
        </button>
        </Wrapper>
      <Formik>
      <Form>
        { encuesta.map((opciones, index) => {
        return (
          <WrapperContainer>
          <label>Pregunta {index + 1} de 5</label><Container key={index} titulo={opciones.pregunta}>
          <SelectOp length={encuesta.length} index= {index} contenido={opciones.opciones} seleccionado={seleccionado} id={itemActual}/>
          </Container>
          </WrapperContainer>
        )
      })}
      </Form>
    </Formik>
    </div>
    )
}